# Hello world


# Configuration
Copy the `.env` file to `.env.local` and replace the `change_me`'s:

# Indexing data to Algolia
`yarn import-data`

The indexing script relies on 3 environment variables to run:
`ALGOLIA_APPLICATION_ID`, `ALGOLIA_ADMIN_API_KEY` and `RESTAURANTS_INDEX_NAME` (also defined in your `.env.local` file)

Define them with the `export` bash function or run the command with the variables inline:
`ALGOLIA_APPLICATION_ID=change_me ALGOLIA_ADMIN_API_KEY=change_me RESTAURANTS_INDEX_NAME=change_me `
