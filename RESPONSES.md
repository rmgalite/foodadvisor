> I'm new to search engines, and there are a lot of concepts I'm not educated on. To make my onboarding smoother, it'd help if you could provide me with some definitions of the following concepts:
>
> - Records
> - Indexing
>
> I'm also struggling with understanding what types of metrics would be useful to include in the "Custom Ranking."
>
> Cheers, George

Hello George,

Thank you for contacting us.

An index is an entity within Algolia where you import the data you want to search (indexing) and perform queries against (search).
Indexing is the process of adding, updating, deleting, or manipulating the data within the index.

Each item, JSON objects, stored in an index is a record.
Each attribute in a record can be used for searching, displaying, filtering, or ranking.

To better understand how Algolia works, you can browse our guides that explain the different concepts: https://www.algolia.com/doc/guides/getting-started/what-is-algolia.

Don't hesitate to come back to us if you have any questions.

Kind regards,

Mael

> Hello,
>
> Sorry to give you the kind of feedback that I know you do not want to hear, but I really hate the new dashboard design. Clearing and deleting indexes are now several clicks away. I am needing to use these features while iterating, so this is inconvenient.
>
> Regards, Matt

Hello Matt,

Thank your for contacting us.

Your feedback is precious to us and it helps us to create the best search experience for our customers. Your feedback has been forwarded to the product design team.

In the meantime, I suggest you to browse our API reference, to help you managing your index programmatically:

- Clearing indexes:
https://www.algolia.com/doc/api-reference/api-methods/clear-index/#examples

- Deleting indexes:
https://www.algolia.com/doc/api-reference/api-methods/delete-index/#examples

Don't hesitate to come back to us if you have any questions,

Best,

Mael

> Hi,
>
> I'm looking to integrate Algolia in my website. Will this be a lot of development work for me? What's the high level process look like?
>
> Regards, Leo

Hi Leo,

This is great news!

The development work depends on a lot of parameters like the organization of your data, what you want to do with Algolia, and the UI of your website/application.

The process looks like this:

- Push your data to algolia (indexing)
- Configure the search behavior (filtering, ranking)
- Set up your search UI

To better understand how Algolia works, you can follow our getting started guide: https://www.algolia.com/doc/tutorials/getting-started/quick-start-with-the-api-client/javascript.
You can also schedule a demo with our team, we're be pleased to hlep you: https://www.algolia.com/schedule-demo.

Don't hesitate to come back to us if you have any questions.

Best,

Mael
