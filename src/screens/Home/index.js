import React, { Component } from 'react'
import algoliasearch from 'algoliasearch'
import algoliasearchHelper from 'algoliasearch-helper'
import QueryString from 'querystring'
import Home from './Home'
import UserLocation from '../../services/user-location'

const FACETS_NAMES = [
  'stars_count2',
  'food_type',
  'payment_options',
  'price_range',
]

export default class HomeScreen extends Component {
  constructor(props) {
    super(props)

    const ALGOLIA_SEARCH_KEY = process.env.REACT_APP_ALGOLIA_SEARCH_KEY
    const ALGOLIA_APPLICATION_ID = process.env.REACT_APP_ALGOLIA_APPLICATION_ID
    const RESTAURANTS_INDEX_NAME = process.env.REACT_APP_RESTAURANTS_INDEX_NAME
    const client = algoliasearch(ALGOLIA_APPLICATION_ID, ALGOLIA_SEARCH_KEY)

    this.searchHelper = algoliasearchHelper(client, RESTAURANTS_INDEX_NAME, {
      disjunctiveFacets: [
        'payment_options',
        'stars_count2',
        'food_type',
        'price_range',
      ],
      aroundLatLngViaIP: true,
    })
    this.searchHelper.on('result', this.handleResults)

    const qs = QueryString.parse(props.history.location.search.slice(1))
    this.searchHelper.setQuery(qs.q)
    this.searchHelper.search()

    if (UserLocation.isAccessible() && UserLocation.isAccessGranted()) {
      UserLocation.getUserLocation(position => this.handleGeoLocation(position))
    }

    this.state = {}
  }

  saveQuery = () => {
    // Save the query
    this.props.history.push({
      search: `${this.getQueryString()}`,
    })
  }

  handleSearchChange = event => {
    this.searchHelper.setQuery(event.target.value).search()
    this.setState({ search: event.target.value })
    this.saveQuery()
  }

  handleResults = content => {
    const { hits, query, nbHits, processingTimeMS, page, nbPages } = content
    const facets = FACETS_NAMES.reduce((acc, facetName) => {
      return {
        ...acc,
        [facetName]: content.getFacetValues(facetName),
      }
    }, {})

    const results = page === 0 ? hits : this.state.results.concat(hits)

    this.setState({
      results,
      facets,
      search: query,
      resultsCount: nbHits,
      processingTime: processingTimeMS,
      hasMorePages: page < nbPages - 1,
    })
  }

  handleFacetClick = (facetName, facetValue) => {
    this.searchHelper.toggleFacetRefinement(facetName, facetValue).search()
    window.scroll(0, 0)
    this.saveQuery()
  }

  handleLoadMore = () => {
    this.searchHelper.nextPage().search()
  }

  handleClearFacetsClick = () => {
    this.searchHelper.clearRefinements().search()
    window.scroll(0, 0)
    this.saveQuery()
  }

  handleGeolocationClick = () => {
    if (UserLocation.isAccessible()) {
      UserLocation.getUserLocation(position => {
        this.handleGeoLocation(position)
      })
    }
  }

  handleGeoLocation = position => {
    const latLng = `${position.coords.latitude},${position.coords.longitude}`

    this.searchHelper
      .setQueryParameter('aroundLatLng', latLng)
      .setQueryParameter('aroundLatLngViaIP', false)
      .search()
  }

  getQueryString = () => {
    const state = this.searchHelper.getState()
    const { facets, query } = state
    return algoliasearchHelper.url.getQueryStringFromState({ facets, query })
  }

  render() {
    return (
      <Home
        search={this.state.search}
        results={this.state.results}
        facets={this.state.facets}
        resultsCount={this.state.resultsCount}
        processingTime={this.state.processingTime}
        hasMorePages={this.state.hasMorePages}
        onSearchChange={this.handleSearchChange}
        onFacetClick={this.handleFacetClick}
        onLoadMoreClick={this.handleLoadMore}
        onClearFacetsClick={this.handleClearFacetsClick}
        onGeolocationClick={this.handleGeolocationClick}
      />
    )
  }
}
