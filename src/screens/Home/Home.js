import React from 'react'
import Sidebar from './Sidebar'
import RestaurantsList from './RestaurantsList'
import SearchButtonIcon from './SearchButtonIcon'
import ScrollTopButton from './ScrolltopButton'
import UserLocation from '../../services/user-location'
import './Home.css'

const Home = ({
  search,
  onSearchChange,
  results,
  facets,
  resultsCount,
  processingTime,
  onFacetClick,
  onLoadMoreClick,
  hasMorePages,
  onClearFacetsClick,
  onGeolocationClick,
}) => {
  return (
    <div className="home">
      <div className="searchBar">
        <input
          type="text"
          className="searchBar-input"
          placeholder="Search for Restaurants by Name, Cuisine, Location"
          value={search || ''}
          onChange={onSearchChange}
        />

        {UserLocation.isAccessible() &&
          !UserLocation.isAccessDenied() &&
          !UserLocation.isAccessGranted() && (
            <SearchButtonIcon onClick={onGeolocationClick} />
          )}
      </div>

      <div className="home-container">
        <Sidebar
          categories={facets}
          onCategoryClick={onFacetClick}
          onClearFiltersClick={onClearFacetsClick}
        />

        <RestaurantsList
          restaurants={results}
          resultsCount={resultsCount}
          processingTime={processingTime}
          hasMorePages={hasMorePages}
          onLoadMoreClick={onLoadMoreClick}
        />

        <ScrollTopButton />
      </div>
    </div>
  )
}

export default Home
