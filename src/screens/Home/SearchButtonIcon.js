import React, { Component } from 'react'
import './SearchButtonIcon.css'

export default class SearchButtonIcon extends Component {
  handleClick = event => {
    event.preventDefault()
    this.props.onClick()
  }

  render() {
    return (
      <a onClick={this.handleClick} className="searchButtonIcon">
        <img
          alt="Search icon"
          src={`data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMzLjI5MyAzMy4yOTMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMzLjI5MyAzMy4yOTM7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMjRweCIgaGVpZ2h0PSIyNHB4Ij4KPGc+Cgk8cGF0aCBkPSJNMTYuNTc4LDMzLjI5M2MtMC4xODcsMC0wLjM2Mi0wLjEwNC0wLjQ0OC0wLjI3N0wwLjA1MiwwLjcyMkMtMC4wNDMsMC41My0wLjAwNSwwLjI5OCwwLjE0NywwLjE0NiAgIHMwLjM4Ni0wLjE4OCwwLjU3Ni0wLjA5NGwzMi4yOTMsMTYuMDc2YzAuMjA3LDAuMTAzLDAuMzE2LDAuMzM1LDAuMjY1LDAuNTZjLTAuMDUyLDAuMjI1LTAuMjUyLDAuMzg2LTAuNDgzLDAuMzg4bC0xNS41ODUsMC4xMzYgICBsLTAuMTM0LDE1LjU4NWMtMC4wMDIsMC4yMzEtMC4xNjIsMC40MzEtMC4zODgsMC40ODNDMTYuNjUzLDMzLjI4OSwxNi42MTUsMzMuMjkzLDE2LjU3OCwzMy4yOTN6IE0xLjYxMiwxLjYxMmwxNC40ODQsMjkuMDkxICAgbDAuMTIxLTEzLjk5MWMwLjAwMi0wLjI3MiwwLjIyMy0wLjQ5MywwLjQ5Ni0wLjQ5NmwxMy45OTEtMC4xMjNMMS42MTIsMS42MTJ6IiBmaWxsPSIjMWM2ODhlIi8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==`}
        />
      </a>
    )
  }
}
