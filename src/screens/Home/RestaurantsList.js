import React, { Component } from 'react'
import Restaurant from './Restaurant'
import './RestaurantsList.css'

export default class RestaurantsList extends Component {
  handleMoreClick = event => {
    event.preventDefault()
    this.props.onLoadMoreClick()
  }

  render() {
    const {
      restaurants = [],
      processingTime,
      resultsCount,
      hasMorePages,
    } = this.props

    return (
      <div className="restaurantsList">
        <div className="restaurantsList-title">
          {typeof resultsCount === 'number' &&
            typeof processingTime === 'number' && (
              <div className="restaurantsList-results">
                {resultsCount} result{resultsCount > 1 && 's'} found in{' '}
                {processingTime / 1000} seconds
              </div>
            )}

          <span>&nbsp;</span>
        </div>

        {restaurants &&
          restaurants.map(restaurant => (
            <Restaurant
              key={restaurant.objectID}
              name={restaurant.name}
              reviewsCount={restaurant.reviews_count}
              starsCount={restaurant.stars_count}
              starsCount2={restaurant.stars_count2}
              category={restaurant.food_type}
              area={restaurant.neighborhood}
              priceRange={restaurant.price_range}
              pictureUrl={restaurant.image_url}
              reserveUrl={restaurant.reserve_url}
            />
          ))}

        {hasMorePages && (
          <div className="restaurantsList-more">
            <a
              className="restaurantsList-more-button"
              onClick={this.handleMoreClick}
            >
              Show more
            </a>
          </div>
        )}
      </div>
    )
  }
}
