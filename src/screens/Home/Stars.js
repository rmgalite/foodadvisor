import React from 'react'
import classnames from 'classnames'
import './Stars.css'

const Stars = ({ count }) => {
  return (
    <div className="stars">
      <div className={classnames('plain-stars', `plain-stars--${count}`)} />
    </div>
  )
}

export default Stars
