import React from 'react'
import Stars from './Stars'
import './Restaurant.css'

const Restaurant = ({
  name,
  reviewsCount,
  starsCount,
  category,
  area,
  priceRange,
  pictureUrl,
  starsCount2,
  reserveUrl,
}) => {
  return (
    <div className="restaurant">
      <div className="restaurant-picture">
        <img src={pictureUrl} alt={name} />
      </div>

      <div className="restaurant-details">
        <div className="restaurant-name">{name}</div>

        <div className="restaurant-reviews">
          <div className="restaurant-starsCount">
            <span className="value">{starsCount}</span>
            <Stars count={starsCount2} />
          </div>
          <div className="restaurant-reviewsCount">
            ({reviewsCount} reviews)
          </div>
        </div>

        <div className="restaurant-moreDetails">
          <div className="restaurant-moreDetails-detail restaurant-moreDetails-detail--first">
            {category}
          </div>
          <div className="restaurant-moreDetails-detail">{area}</div>
          <div className="restaurant-moreDetails-detail restaurant-moreDetails-detail--last">
            {priceRange}
          </div>
        </div>

        <a href={reserveUrl} target="_blank" className="restaurant-reserve-url">
          Make a reservation &raquo;
        </a>
      </div>
    </div>
  )
}

export default Restaurant
