import React, { Component } from 'react'
import classnames from 'classnames'
import Stars from './Stars'
import './Sidebar.css'

const FOOD_TYPES_COUNT = 10

export default class Sidebar extends Component {
  state = { hidden: false, displayAllFoodTypes: false }

  handleToggleHidden = event => {
    event.preventDefault()
    this.setState({ hidden: !this.state.hidden })
  }

  handleCategoryClick = (categoryName, value, event) => {
    event.preventDefault()
    this.props.onCategoryClick(categoryName, value)
  }

  handleMoreFoodTypesClick = event => {
    event.preventDefault()
    this.setState({ displayAllFoodTypes: true })
  }

  handleClearFiltersClick = event => {
    event.preventDefault()
    this.props.onClearFiltersClick()
  }

  render() {
    const { categories } = this.props

    if (!categories) {
      return null
    }

    const foodTypes = this.state.displayAllFoodTypes
      ? categories.food_type
      : categories.food_type.slice(0, FOOD_TYPES_COUNT)

    const starsCount = categories.stars_count2.sort((rating1, rating2) => {
      return rating1.name - rating2.name
    })

    return (
      <div className="sidebar">
        <a className="sidebar-toggle" onClick={this.handleToggleHidden}>
          {this.state.hidden ? 'Show' : 'Hide'} filters
        </a>

        {!this.state.hidden && (
          <div>
            <div>
              <div className="category-label">Cuisine / Food type</div>
              <div className="category-values category-values--food-type">
                {foodTypes.map(foodType => (
                  <a
                    key={`food_type-${foodType.name}`}
                    className={classnames(
                      'category-value',
                      foodType.isRefined && 'category-value--selected'
                    )}
                    onClick={this.handleCategoryClick.bind(
                      this,
                      'food_type',
                      foodType.name
                    )}
                  >
                    <span>{foodType.name}</span>
                    <span>{foodType.count}</span>
                  </a>
                ))}

                {categories.food_type.length > FOOD_TYPES_COUNT &&
                  !this.state.displayAllFoodTypes && (
                    <a
                      onClick={this.handleMoreFoodTypesClick}
                      className="category-values-link"
                    >
                      Show more...
                    </a>
                  )}
              </div>

              <div className="category-values category-values--all">
                {categories.food_type.map(foodType => (
                  <a
                    key={`food_type-${foodType.name}`}
                    className={classnames(
                      'category-value',
                      foodType.isRefined && 'category-value--selected'
                    )}
                    onClick={this.handleCategoryClick.bind(
                      this,
                      'food_type',
                      foodType.name
                    )}
                  >
                    <span>{foodType.name}</span>
                    <span>{foodType.count}</span>
                  </a>
                ))}
              </div>
            </div>

            <div>
              <div className="category-label">Rating</div>
              <div className="category-values">
                {starsCount.map(rating => (
                  <a
                    key={`stars_count2-${rating.name}`}
                    className={classnames(
                      'category-value',
                      'category-value--bordered',
                      rating.isRefined && 'category-value--selected'
                    )}
                    onClick={this.handleCategoryClick.bind(
                      this,
                      'stars_count2',
                      rating.name
                    )}
                  >
                    <Stars count={rating.name} />
                  </a>
                ))}
              </div>
            </div>

            <div>
              <div className="category-label">Price range</div>
              <div className="category-values">
                {categories.price_range.map(category => (
                  <a
                    key={`price_range-${category.name}`}
                    className={classnames(
                      'category-value',
                      category.isRefined && 'category-value--selected'
                    )}
                    onClick={this.handleCategoryClick.bind(
                      this,
                      'price_range',
                      category.name
                    )}
                  >
                    <span>{category.name}</span>
                    <span>{category.count}</span>
                  </a>
                ))}
              </div>
            </div>

            <div>
              <div className="category-label">Payment options</div>
              <div className="category-values">
                {categories.payment_options.map(category => (
                  <a
                    key={`payment_options-${category.name}`}
                    className={classnames(
                      'category-value',
                      category.isRefined && 'category-value--selected'
                    )}
                    onClick={this.handleCategoryClick.bind(
                      this,
                      'payment_options',
                      category.name
                    )}
                  >
                    <span>{category.name}</span>
                    <span>{category.count}</span>
                  </a>
                ))}
              </div>
            </div>

            <a
              className="category-values-link"
              onClick={this.handleClearFiltersClick}
            >
              Clear filters
            </a>
          </div>
        )}
      </div>
    )
  }
}
