import React, { Component } from 'react'
import createHistory from 'history/createBrowserHistory'

import Home from './screens/Home'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.history = createHistory()
  }

  render() {
    return (
      <div className="App">
        <Home history={this.history} />
      </div>
    )
  }
}

export default App
