const noop = () => {}

const UserLocation = {
  isAccessDenied: () => {
    const accessStatus = JSON.parse(
      localStorage.getItem('@CoolPlacesToEat.locationAccessStatus')
    )

    return accessStatus === 'DENIED'
  },

  isAccessGranted: () => {
    const accessStatus = JSON.parse(
      localStorage.getItem('@CoolPlacesToEat.locationAccessStatus')
    )

    return accessStatus === 'GRANTED'
  },

  isAccessible: () => {
    return !!navigator.geolocation
  },

  setAccessStatus: accessStatus => {
    localStorage.setItem(
      '@CoolPlacesToEat.locationAccessStatus',
      JSON.stringify(accessStatus)
    )
  },

  getUserLocation: (onSuccess = noop, onError = noop) => {
    navigator.geolocation.getCurrentPosition(
      position => {
        // Success
        UserLocation.setAccessStatus('GRANTED')
        onSuccess(position)
      },
      error => {
        // Error
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
        if (error.code === 1) {
          UserLocation.setAccessStatus('DENIED')
        } else {
          console.error('Could not find the user position', error)
        }
        onError(error)
      }
    )
  },
}

export default UserLocation
