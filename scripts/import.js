import algoliasearch from 'algoliasearch'
import csv from 'csv-parser'
import fs from 'fs'
import path from 'path'

const RESTAURANTS_INDEX_NAME = process.env.RESTAURANTS_INDEX_NAME
const ALGOLIA_APPLICATION_ID = process.env.ALGOLIA_APPLICATION_ID
const ALGOLIA_ADMIN_API_KEY = process.env.ALGOLIA_ADMIN_API_KEY
const RESTAURANTS_JSON_FILE_PATH = './restaurants_list.json'

const ALLOWED_PAYMENT_OPTIONS = {
  AMEX: 'AMEX',
  'Diners Club': 'Discover',
  Discover: 'Discover',
  MasterCard: 'MasterCard',
  Visa: 'Visa',
  'Carte Blanche': 'Discover',
}

const sanitizePaymentOptions = paymentOptions => {
  return (
    paymentOptions
      // Ensure to use only allowed options
      .map(option => ALLOWED_PAYMENT_OPTIONS[option])
      // Remove null values and duplicates
      .filter((option, index, arr) => !!option && arr.indexOf(option) === index)
  )
}

const sanitizePriceRange = option => {}

const importData = () =>
  new Promise((resolve, reject) => {
    const jsonData = require(RESTAURANTS_JSON_FILE_PATH)
    const records = jsonData.reduce((acc, json) => {
      const result = Object.assign(json, {
        payment_options: sanitizePaymentOptions(json.payment_options),
      })

      return Object.assign(acc, {
        [json.objectID]: result,
      })
    }, {})

    const csvOptions = {
      separator: ';',
    }

    fs.createReadStream(path.join(__dirname, 'restaurants_info.csv'))
      .pipe(csv(csvOptions))
      .on('data', data =>
        Object.assign(records[data.objectID], data, {
          stars_count2: Math.floor(data.stars_count),
        })
      )
      .on('end', () => {
        const client = algoliasearch(
          ALGOLIA_APPLICATION_ID,
          ALGOLIA_ADMIN_API_KEY,
          {
            timeouts: {
              connect: 60 * 1000,
              read: 60 * 1000,
              write: 60 * 1000,
            },
          }
        )
        const index = client.initIndex(RESTAURANTS_INDEX_NAME)

        console.log(`Indexing ${jsonData.length} restaurants...`)

        index.addObjects(jsonData, (err, content) => {
          if (err) {
            reject(err)
            return
          }

          resolve()
        })
      })
  })

importData()
  .then(() => {
    console.log(
      `Your data has been successfully indexed on the algolia index "${RESTAURANTS_INDEX_NAME}"`
    )
    process.exit(0)
  })
  .catch(err => {
    console.error('Oops. The data has not been indexed:', err.message)
    process.exit(1)
  })
